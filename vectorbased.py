from typing import Union
from string import ascii_lowercase

class VectorBasedTable:

    def __init__(self):
        self.table = {}

    def hash_string(self, string_to_hash: str) -> tuple:
        result = []
        for letter in ascii_lowercase:
            result.append(string_to_hash.count(letter))
        return tuple(result)

    def add(self, new_string: str):
        '''Adds correct string to hash table'''
        self.table[self.hash_string(new_string.lower())] = new_string.lower()

    def check(self, string_to_check: str) -> Union[str, None]:
        '''Returns correct string or None when string_to_check has no anagram in hash table'''
        return self.table.get(self.hash_string(string_to_check.lower()))