import unittest
from stringbased import StringBasedTable


class TestStringBasedTable(unittest.TestCase):

    def test_hash_string(self):
        match = StringBasedTable()
        self.assertEqual(match.hash_string('aba'), 'aab')

    def test_add(self):
        match = StringBasedTable()
        match.add('baba')
        self.assertEqual(match.table.get('aabb'), 'baba')

    def test_check(self):
        match = StringBasedTable()
        match.add('baba')
        self.assertEqual(match.check('abab'), 'baba')
        self.assertEqual(match.check('aba'), None)

    def test_ignore_capital_letters(self):
        match = StringBasedTable()
        match.add('baBa')
        self.assertEqual(match.check('Abab'), 'baba')
