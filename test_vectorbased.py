import unittest
from vectorbased import VectorBasedTable


class TestVectorBasedTable(unittest.TestCase):

    def test_hash_string(self):
        match = VectorBasedTable()
        self.assertEqual(match.hash_string('aba'), (2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))

    def test_add(self):
        match = VectorBasedTable()
        match.add('baba')
        self.assertEqual(match.table.get((2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                         0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)), 'baba')

    def test_check(self):
        match = VectorBasedTable()
        match.add('baba')
        self.assertEqual(match.check('abab'), 'baba')
        self.assertEqual(match.check('aba'), None)

    def test_ignore_capital_letters(self):
        match = VectorBasedTable()
        match.add('baBa')
        self.assertEqual(match.check('Abab'), 'baba')
